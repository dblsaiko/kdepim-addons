/*
  SPDX-FileCopyrightText: 2021 Laurent Montel <montel@kde.org>

  SPDX-License-Identifier: GPL-2.0-only
*/

#include "mailmergemenu.h"

MailMergeMenu::MailMergeMenu(QWidget *parent)
    : QMenu(parent)
{
}

MailMergeMenu::~MailMergeMenu()
{
}
